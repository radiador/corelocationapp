//
//  ViewController.swift
//  CoreLocationApp
//
//  Created by formador on 3/4/19.
//  Copyright © 2019 formador. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Contacts

class ViewController: UIViewController {

    @IBOutlet weak var whereIAmButton: UIView!
    @IBOutlet weak var latitudLb: UILabel!
    @IBOutlet weak var longitudLb: UILabel!
    @IBOutlet weak var locationLb: UILabel!
    
    @IBOutlet weak var mapView: MKMapView!
    
    var originalBackgroundColorButton: UIColor?
    
    var firstAppear = true
    
    var locations = [Int: CLLocation]()
    var localities = [Int: String]()

    let locationManager = CLLocationManager()
    let geocoder = CLGeocoder()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        originalBackgroundColorButton = whereIAmButton.backgroundColor
        
        let tapGestureAction = UITapGestureRecognizer(target: self, action: #selector(ViewController.tapInWhereIAmButton(gestureRecognizer:)))
        
        whereIAmButton.addGestureRecognizer(tapGestureAction)
        
        locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
        locationManager.delegate = self
        
        let madridLocation = CLLocationCoordinate2D(latitude: 40.41, longitude: -3.70)
        
        centerMapOnLocation(location: madridLocation)
        
        let leon = FavoriteTown(title: "Leon", subtitle: "Castilla y Leon", coordinate: CLLocationCoordinate2D(latitude: 42, longitude: -5))
        
        let alcala = FavoriteTown(title: "Alcala", subtitle: "Patrimonio de La Humanidad", coordinate: CLLocationCoordinate2D(latitude: 40, longitude: -3))

        let santander = FavoriteTown(title: "Santander", subtitle: "Tiene playa", coordinate: CLLocationCoordinate2D(latitude: 43, longitude: -3.8))

        let barcelona = FavoriteTown(title: "Barcelona", subtitle: "Capital de Cataluña", coordinate: CLLocationCoordinate2D(latitude: 41, longitude: 2))

        
        mapView.addAnnotations([leon, alcala, santander, barcelona])
    }
    
    private func centerMapOnLocation(location: CLLocationCoordinate2D) {
        
        let region = MKCoordinateRegion(center: location, latitudinalMeters: 50000, longitudinalMeters: 50000);

        mapView.setRegion(region, animated: true)
    }
    
    @objc func tapInWhereIAmButton(gestureRecognizer: UITapGestureRecognizer) {
        
        whereIAmButton.backgroundColor = originalBackgroundColorButton
        
        if CLLocationManager.locationServicesEnabled() {
            
            if CLLocationManager.authorizationStatus() == .denied || CLLocationManager.authorizationStatus() == .notDetermined || CLLocationManager.authorizationStatus() == .restricted {
                
                locationManager.requestWhenInUseAuthorization()
            } else {
                
                locationManager.startUpdatingLocation()
            }
            
        } else {
            
            let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)
            
            let alertController = UIAlertController(title: "ATENCION", message: "Ahora mismo no esta disponible la localiación", preferredStyle: .alert)
            
            alertController.addAction(okAction)
            
            present(alertController, animated: true, completion: nil)
        }
    }
}


extension ViewController: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        locationManager.stopUpdatingLocation()
        
        let lastLocation = locations.last
        
        guard let location = lastLocation else { return }
        
        centerMapOnLocation(location: location.coordinate)
        
        latitudLb.text = location.coordinate.latitude.description
        longitudLb.text = location.coordinate.longitude.description
        
        self.locationLb.text = ""
        
        geocoder.reverseGeocodeLocation(location) { placemarks, error in
            
            let firstLocation = placemarks?[0]
            
            self.locationLb.text = firstLocation?.locality
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        
        let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)

        let alertController = UIAlertController(title: "ATENCION", message: "Hay algun tipo de problema", preferredStyle: .alert)

        alertController.addAction(okAction)

        present(alertController, animated: true, completion: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if firstAppear {
            firstAppear = false
            return
        }
        
        if status == .authorizedWhenInUse {
            
            locationManager.startUpdatingLocation()
        } else {
            
            let okAction = UIAlertAction(title: "ok", style: .default, handler: nil)
            
            let alertController = UIAlertController(title: "ATENCION", message: "Porfavor, activa los servicios de localizacion", preferredStyle: .alert)
            
            alertController.addAction(okAction)
            
            present(alertController, animated: true, completion: nil)
        }
    }
}

extension ViewController: MKMapViewDelegate {
    
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        guard let annotation = annotation as? FavoriteTown else { return nil }
        
        let identifier = "favouriteMarkIdentifier"
        
        var view: MKMarkerAnnotationView
        
        if let dequeuedView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKMarkerAnnotationView {
            
            dequeuedView.annotation = annotation
            view = dequeuedView
        } else {
            
            view = MKMarkerAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            view.canShowCallout = true
            view.calloutOffset = CGPoint(x: -5 , y: 5)
            view.rightCalloutAccessoryView = UIButton(type: .detailDisclosure)
        }
        
        return view
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl) {
        
        let location = view.annotation as? FavoriteTown
        
        if let location = location {
            
            let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
            
            location.mapItem().openInMaps(launchOptions: launchOptions)
        }
    }
    
}

class FavoriteTown: NSObject, MKAnnotation {
    
    let title: String?
    let subtitle: String?
    let street: String
    
    let coordinate: CLLocationCoordinate2D
    
    init(title: String, subtitle: String, coordinate: CLLocationCoordinate2D, street: String = "calle mayor") {
        
        self.title = title
        self.subtitle = subtitle
        self.coordinate = coordinate
        self.street = street
    }
    
    func mapItem() -> MKMapItem {
        
        let addresDic = [CNPostalAddressStreetKey: street]
        let placemark = MKPlacemark(coordinate: coordinate, addressDictionary: addresDic)
        let mapItem = MKMapItem(placemark: placemark)
        mapItem.name = title
        
        return mapItem
    }
}

